const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];



const list = document.createElement('ul');

books.forEach(book => {
  const {author, name, price} = book;
  const listItem = document.createElement('li');

  checkObjProp(author, 'Author', listItem);
  checkObjProp(name, 'Name', listItem);
  checkObjProp(price, 'Price', listItem);
  
  list.append(listItem);
});

document.getElementById('root').append(list);



function checkObjProp(prop, label, elem) {
  try {
    const p = document.createElement('p');
    p.innerText = `${label}: ${prop}`;
    if (!prop) throw new Error(`${label} is not defined`);
    elem.append(p);
  } catch(e) {
    console.log(e.message);
  }
}